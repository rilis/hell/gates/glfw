## GLFW proxy repository for hell

This is proxy repository for [GLFW library](http://www.glfw.org/), which allow you to build and install it  using [hell dependency manager](https://gitlab.com/rilis/hell/hell).

* If you have problem with installation of GLFW using hell, have improvement idea, or want to request support for other versions of GLFW, then please [create issue here](https://gitlab.com/rilis/rilis/issues).
* If you found bug in GLFW itself please create issue on [GLFW issue tracker](https://github.com/glfw/glfw/issues), because here we don't do any kind of GLFW development.
